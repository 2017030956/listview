package com.example.listview_personalizado;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.view.menu.MenuView;

import java.util.ArrayList;
import java.util.List;

public class SpinnerAdapter extends ArrayAdapter<ItemData>{
    int groupId;
    Activity Context;
    ArrayList<ItemData> list;
    LayoutInflater inflater;
    private Filter filter;


    public SpinnerAdapter(Activity Context, int groupId, int id, ArrayList<ItemData> list){
        super(Context,id,list);
        this.list = list;
        inflater = (LayoutInflater)Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View itemView = inflater.inflate(groupId,parent,false);
        ImageView imagen = (ImageView) itemView.findViewById(R.id.imgContacto);
        imagen.setImageResource(list.get(position).getImageId());
        TextView textNombre = (TextView)itemView.findViewById(R.id.lblNombre);
        textNombre.setText(list.get(position).getTextNombre());
        TextView txtfras = (TextView)itemView.findViewById(R.id.lblfrace);
        txtfras.setText(list.get(position).getTxtfras());
        return  itemView;
    }

    public View  getDropDownView(int position, View convertView, ViewGroup parent){
        return getView(position,convertView,parent);
    }

    public Filter getFilter(){
        if (filter == null){
            filter = new AppFilter<ItemData>(list);
        }
        return filter;
    }

    private class AppFilter<T> extends Filter{

        private ArrayList<ItemData> sourceObjets;

        public AppFilter(ArrayList<ItemData> objects){
            sourceObjets = new ArrayList<>();
            synchronized (this){
                sourceObjets.addAll(objects);
            }
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterSeq = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();

            if(filterSeq !=null && filterSeq.length()>0){
                ArrayList<ItemData> filter = new ArrayList<ItemData>();

                for (ItemData object:sourceObjets){
                    if(object.getTextNombre().toLowerCase().contains(filterSeq)){

                        filter.add(object);
                    }
                }
                results.count = filter.size();
                results.values = filter;
            }
            else {
                synchronized (this){
                    results.values= sourceObjets;
                    results.count = sourceObjets.size();
                }
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            ArrayList<T> filtered = (ArrayList<T>)results.values;
            notifyDataSetChanged();
            clear();
            for (int i = 0,l = filtered.size();i < l ;i++){
                add((ItemData)filtered.get(i));
                notifyDataSetInvalidated();
            }
        }
    }
}
