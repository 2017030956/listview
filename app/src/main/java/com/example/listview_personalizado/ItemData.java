package com.example.listview_personalizado;

import android.content.Intent;

public class ItemData {

    private String textNombre;
    private Integer imageId;
    private String txtfras;

    public ItemData(String textNombre, Integer imageId, String txtfras) {
        this.textNombre = textNombre;
        this.imageId = imageId;
        this.txtfras= txtfras;
    }

    public ItemData() {
    }

    public String getTextNombre() {
        return textNombre;
    }

    public void setTextNombre(String textNombre) {
        this.textNombre = textNombre;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public String getTxtfras() {
        return txtfras;
    }

    public void setTxtfras(String txtfras) {
        this.txtfras = txtfras;
    }
}
