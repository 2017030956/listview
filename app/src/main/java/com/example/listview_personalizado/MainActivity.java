package com.example.listview_personalizado;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private ListView lst;
    private SearchView srcLista;
    private ArrayList<String> arrayList;
    private ArrayList<String> arrFras;
    private SpinnerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lst = (ListView)findViewById(R.id.lstNombres);
        srcLista = (SearchView)findViewById(R.id.menu_search);
        arrayList = new ArrayList<>();
        arrFras = new ArrayList<>();
        arrayList.addAll(Arrays.asList(getResources().getStringArray(R.array.array_nombre)));
        arrFras.addAll(Arrays.asList(getResources().getStringArray(R.array.array_fras)));
        ArrayList<ItemData> list = new ArrayList<>();
        for (int i=0; i < arrayList.size();i++){
            list.add(new ItemData(arrayList.get(i).toString(),R.drawable.dolphin,arrFras.get(i).toString()));

        }
        adapter = new SpinnerAdapter(this,R.layout.listview_layout,R.id.lblNombre,list);
        lst.setAdapter(adapter);
        lst.setTextFilterEnabled(true);
        lst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(parent.getContext(),getString(R.string.msgSeleccionado).toString() +" "+((ItemData)parent.getItemAtPosition(position)).getTextNombre(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);

        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView)menuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}
